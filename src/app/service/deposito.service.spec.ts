/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DepositoService } from './deposito.service';

describe('Service: Deposito', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepositoService]
    });
  });

  it('should ...', inject([DepositoService], (service: DepositoService) => {
    expect(service).toBeTruthy();
  }));
});
