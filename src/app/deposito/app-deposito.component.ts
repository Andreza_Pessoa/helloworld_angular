import { Component, EventEmitter, Output } from "@angular/core";
import { DepositoService } from "../service/deposito.service";


@Component({
    selector: "app-deposito",
    templateUrl: "./app-deposito.component.html",
    styleUrls: ["./app-deposito.component.scss"]

})
export class AppDeposito {

  //  @Output() aoDepositar = new EventEmitter<any>()

    conta: number = 0;
    valor: number = 0;

    constructor(private service : DepositoService){

    }

    public depositar() {
        console.log("depositou!! ");
        console.log("conta: " + this.conta);
        console.log("valor: " + this.valor);

        const deposito = {valor: this.valor, conta: this.conta}
       // this.aoDepositar.emit(deposito)

       this.service.addDeposito(deposito).subscribe(resultado =>{
         console.log(resultado);
         this.limparTela()
       },error =>{
         console.error(error)
       })
    }

    private limparTela(){
        this.conta = 0;
        this.valor = 0;
    }

    // public teste(){
    //   console.log("clicou no botao teste");

    // }
}
