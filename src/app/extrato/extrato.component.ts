import { Component, Input, OnInit } from '@angular/core';
import { Deposito } from 'models/deposito.models';
import { DepositoService } from '../service/deposito.service';

@Component({
  selector: 'app-extrato',
  templateUrl: './extrato.component.html',
  styleUrls: ['./extrato.component.scss']
})
export class ExtratoComponent implements OnInit {

  //  @Input() depositos : any[] = [];
  depositos : any[] = [];
  constructor(private service : DepositoService) { }

   ngOnInit(): void {
    //pegar os dados
   // this.depositos = this.service.listaDepositos

   //faz uma chamada para receber os dados
    this.service.getAllDeposito().subscribe((depositosPP: Deposito[]) =>{

       console.table(depositosPP)
       this.depositos = depositosPP

     })
  }

}
